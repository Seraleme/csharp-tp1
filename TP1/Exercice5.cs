using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace TP1
{
    public class Exercice5
    {
        public static void Lunch()
        {
            using (StreamReader r =
                new StreamReader("/home/severin/Workspace/c#/TP1/TP1/DOGE_AllDataPoints_3Days.json"))
            {
                string json = r.ReadToEnd();
                Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(json);

                // Example : Print the value of the key 'config.data', the value should be "assets"
                if (myDeserializedClass != null)
                    Console.WriteLine(myDeserializedClass.Config.Data);
            }
        }

        // Genrate on https://json2csharp.com/ 
        public class Config
        {
            public string Data { get; set; }
            public int DataPoints { get; set; }
            public string Interval { get; set; }
            public string Symbol { get; set; }
        }

        public class Usage
        {
            public int Day { get; set; }
            public int Month { get; set; }
        }

        public class TimeSery
        {
            public int AssetId { get; set; }
            public int Time { get; set; }
            public double Open { get; set; }
            public double Close { get; set; }
            public double High { get; set; }
            public double Low { get; set; }
            public double Volume { get; set; }
            public object MarketCap { get; set; }
            public int UrlShares { get; set; }
            public int UniqueUrlShares { get; set; }
            public int RedditPosts { get; set; }
            public int RedditPostsScore { get; set; }
            public int RedditComments { get; set; }
            public int RedditCommentsScore { get; set; }
            public int Tweets { get; set; }
            public int TweetSpam { get; set; }
            public int TweetFollowers { get; set; }
            public int TweetQuotes { get; set; }
            public int TweetRetweets { get; set; }
            public int TweetReplies { get; set; }
            public int TweetFavorites { get; set; }
            public int TweetSentiment1 { get; set; }
            public int TweetSentiment2 { get; set; }
            public int TweetSentiment3 { get; set; }
            public int TweetSentiment4 { get; set; }
            public int TweetSentiment5 { get; set; }
            public int TweetSentimentImpact1 { get; set; }
            public int TweetSentimentImpact2 { get; set; }
            public int TweetSentimentImpact3 { get; set; }
            public int TweetSentimentImpact4 { get; set; }
            public int TweetSentimentImpact5 { get; set; }
            public int SocialScore { get; set; }
            public double AverageSentiment { get; set; }
            public int SentimentAbsolute { get; set; }
            public int SentimentRelative { get; set; }
            public int News { get; set; }
            public double PriceScore { get; set; }
            public double SocialImpactScore { get; set; }
            public double CorrelationRank { get; set; }
            public double GalaxyScore { get; set; }
            public double Volatility { get; set; }
            public int AltRank { get; set; }
            public int AltRank30d { get; set; }
            public double AltRankHourAverage { get; set; }
            public int MarketCapRank { get; set; }
            public int PercentChange24hRank { get; set; }
            public int Volume24hRank { get; set; }
            public int SocialVolume24hRank { get; set; }
            public int SocialScore24hRank { get; set; }
            public int SocialContributors { get; set; }
            public int SocialVolume { get; set; }
            public double PriceBtc { get; set; }
            public int SocialVolumeGlobal { get; set; }
            public double SocialDominance { get; set; }
            public object MarketCapGlobal { get; set; }
            public double MarketDominance { get; set; }
            public double PercentChange24h { get; set; }
        }

        public class Datum
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Symbol { get; set; }
            public double Price { get; set; }
            public double PriceBtc { get; set; }
            public long MarketCap { get; set; }
            public double PercentChange24h { get; set; }
            public double PercentChange7d { get; set; }
            public double PercentChange30d { get; set; }
            public double Volume24h { get; set; }
            public int SocialContributorsCalc24hPrevious { get; set; }
            public int UrlSharesCalc24hPrevious { get; set; }
            public int TweetSpamCalc24hPrevious { get; set; }
            public int NewsCalc24hPrevious { get; set; }
            public double AverageSentimentCalc24hPrevious { get; set; }
            public int SocialScoreCalc24hPrevious { get; set; }
            public int SocialVolumeCalc24hPrevious { get; set; }
            public int AltRank30dCalc24hPrevious { get; set; }
            public int AltRankCalc24hPrevious { get; set; }
            public int SocialContributorsCalc24h { get; set; }
            public double SocialContributorsCalc24hPercent { get; set; }
            public int UrlSharesCalc24h { get; set; }
            public double UrlSharesCalc24hPercent { get; set; }
            public int TweetSpamCalc24h { get; set; }
            public double TweetSpamCalc24hPercent { get; set; }
            public int NewsCalc24h { get; set; }
            public double NewsCalc24hPercent { get; set; }
            public double AverageSentimentCalc24h { get; set; }
            public double AverageSentimentCalc24hPercent { get; set; }
            public int SocialScoreCalc24h { get; set; }
            public double SocialScoreCalc24hPercent { get; set; }
            public int SocialVolumeCalc24h { get; set; }
            public double SocialVolumeCalc24hPercent { get; set; }
            public int AssetId { get; set; }
            public int Time { get; set; }
            public double Open { get; set; }
            public double High { get; set; }
            public double Low { get; set; }
            public double Volume { get; set; }
            public int UrlShares { get; set; }
            public int UniqueUrlShares { get; set; }
            public int RedditPosts { get; set; }
            public int RedditPostsScore { get; set; }
            public int RedditComments { get; set; }
            public int RedditCommentsScore { get; set; }
            public int Tweets { get; set; }
            public int TweetSpam { get; set; }
            public int TweetFollowers { get; set; }
            public int TweetQuotes { get; set; }
            public int TweetRetweets { get; set; }
            public int TweetReplies { get; set; }
            public int TweetFavorites { get; set; }
            public int TweetSentiment1 { get; set; }
            public int TweetSentiment2 { get; set; }
            public int TweetSentiment3 { get; set; }
            public int TweetSentiment4 { get; set; }
            public int TweetSentiment5 { get; set; }
            public int TweetSentimentImpact1 { get; set; }
            public int TweetSentimentImpact2 { get; set; }
            public int TweetSentimentImpact3 { get; set; }
            public int TweetSentimentImpact4 { get; set; }
            public int TweetSentimentImpact5 { get; set; }
            public int SocialScore { get; set; }
            public double AverageSentiment { get; set; }
            public int SentimentAbsolute { get; set; }
            public int SentimentRelative { get; set; }
            public int News { get; set; }
            public double PriceScore { get; set; }
            public double SocialImpactScore { get; set; }
            public double CorrelationRank { get; set; }
            public double GalaxyScore { get; set; }
            public double Volatility { get; set; }
            public int AltRank { get; set; }
            public int AltRank30d { get; set; }
            public double AltRankHourAverage { get; set; }
            public int MarketCapRank { get; set; }
            public int PercentChange24hRank { get; set; }
            public int Volume24hRank { get; set; }
            public int SocialVolume24hRank { get; set; }
            public int SocialScore24hRank { get; set; }
            public int SocialContributors { get; set; }
            public int SocialVolume { get; set; }
            public int SocialVolumeGlobal { get; set; }
            public double SocialDominance { get; set; }
            public long MarketCapGlobal { get; set; }
            public double MarketDominance { get; set; }
            public string Tags { get; set; }
            public double Close { get; set; }
            public List<TimeSery> TimeSeries { get; set; }
        }

        public class Root
        {
            public Config Config { get; set; }
            public Usage Usage { get; set; }
            public List<Datum> Data { get; set; }
        }
    }
}